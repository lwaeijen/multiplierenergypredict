#!/usr/bin/env python
import numpy as np
import pandas as pd
from math import log, ceil
import re
from math import sqrt
import pickle
from sklearn.metrics import r2_score

#######################################################
#
# Metrics
#


def hamming(a, b, l=5):
    MASK = pow(2, l)-1
    xor = (a ^ b) & MASK
    return float(bin(xor)[2:].count('1'))/float(l)


def mew(*args):
    l = 5
    mew = 0
    for i in args:
        mew = max(mew, ceil(log(abs(int(i))+1, 2)))
    return max(int(mew), 1)/float(l)


def lead_match(a, b, l=5):
    lead_regex = re.compile('^(0*).*$')
    MASK = pow(2, l)-1
    xor = (a ^ b) & MASK
    xor_adj = bin(xor)[2:].zfill(l)
    m = lead_regex.match(xor_adj)
    if m:
        return float(len(m.group(1)))/float(l)
    return 0.0


def zeroes(a, l=5):
    return float(bin(a)[2:].zfill(l).count('0'))/float(l)


def diff(a, b, l=5):
    return (a-b)/float(pow(2, l))


def adiff(a, b):
    return abs(diff(a, b))


def sign(a):
    return 1.0 if (a < 0) else 0.0


def dsign(a, b, c, d):
    s = 0.0
    s += .5 if (sign(a) == sign(c)) else 0
    s += .5 if (sign(b) == sign(d)) else 0
    return s


def xor_sign(a, b, c, d):
    s1 = 1 if (sign(a) == sign(c)) else 0
    s2 = 1 if (sign(b) == sign(d)) else 0
    return float(s1 ^ s2)


#######################################################
#
# Statistics
#
def mean(x):
    return sum(x)/len(x)


def stddev(x, u=None):
    ux = mean(x) if u is None else u
    return sqrt(sum([pow(xi - ux, 2) for xi in x]))


def safe_stddev(x, u=None):
    s = stddev(x, u=u)
    return 1e-13 if s == 0.0 else s


def correlate(x, y):
    ux = mean(x)
    uy = mean(y)
    sx = safe_stddev(x, u=ux)
    sy = safe_stddev(y, u=uy)
    return sum([(xi-ux)*(yi-uy) for xi, yi in zip(x, y)])/(sx*sy)

#######################################################
#
# Machine Learning
#


class MLP(object):
    def __init__(self, hidden_layers=None, fname=None):
        if fname:
            # Load self from previous save
            self.load(fname)
            return

        if hidden_layers:
            # Define the MLP
            from sklearn.neural_network import MLPRegressor
            self.mlp = MLPRegressor(
                hidden_layer_sizes=hidden_layers,
                max_iter=2000,
                activation='relu',
                verbose=True,
                learning_rate_init=0.008,
                learning_rate='adaptive'
            )

    def fit(self, X, y, test_size=0.9):
        # Perceptron Classifier (Artificial Neural Net) - Results are dissapointing
        from sklearn.model_selection import train_test_split

        # split data into training and test sets
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=test_size, random_state=1337
        )

        # scale the inputs
        from sklearn.preprocessing import StandardScaler
        self.scaler = StandardScaler()
        self.scaler.fit(X_train)
        X_train = self.scaler.transform(X_train)
        X_test = self.scaler.transform(X_test)

        # train the MLP
        self.mlp.fit(X_train, y_train)

    def eval(self, X):
        # classify the complete data set
        return self.mlp.predict(self.scaler.transform(X))

    def r2(self, X, y):
        from sklearn.metrics import r2_score
        return r2_score(y, self.eval(X))

    def save(self, fname='mlp.p'):
        with open(fname, 'wb') as f:
            pickle.dump(self, f)

    def load(self, fname='mlp.p'):
        with open(fname, 'rb') as f:
            self.__dict__.update(pickle.load(f).__dict__)


#######################################################
#
# Plotting
#
def heatmap(measured, predicted, metricname='MLP', fname=None, show=True):
    import matplotlib.pyplot as plt
    import matplotlib

    # To numpy
    x = np.asarray(predicted)
    y = np.asarray(measured)

    # Scale things
    # x=np.multiply(x,1000)
    # y=np.multiply(y,1000)

    # generate heatmap
    bins = min(100, len(set(x)))
    heatmap, xedges, yedges = np.histogram2d(x, y, bins=(bins, bins))
    extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]

    # plot da heatmap
    plt.clf()
    matplotlib.rcParams.update({'font.size': 28})
    plt.imshow(heatmap.T, interpolation='nearest',
               origin='lower', aspect='auto', extent=extent)

    plt.ylabel('Energy pJ/op')
    plt.xlabel(metricname)

    # remove x ticks
    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False  # labels along the bottom edge are off
    )

    # plt.gcf().subplots_adjust(bottom=0.50, left=0.15)
    # plt.gcf().set_size_inches(5*1.4, 8*1.4)

    if fname:
        plt.savefig(fname)

    if show:
        plt.show()

    return plt


#######################################################
#
# Main
#
if __name__ == '__main__':
    import argparse

    # Construct the parser
    parser = argparse.ArgumentParser(
        description="Multiplication Energy Prediction Tool")
    parser.add_argument(
        '-i', '--input', default='energy.csv', dest='ifile',
        help="Csv file with energy measurements"
    )
    parser.add_argument(
        '-e', '--energy', default='energy.png',
        help="Output file for perfect energy vs energy plot"
    )
    parser.add_argument(
        '-m', '--mlp', default='mlp.png',
        help="Output file for MLP prediction plot "
    )
    parser.add_argument(
        '-l', '--linreg', default='linreg.png',
        help="Output file for Linear Regression prediction plot"
    )
    parser.add_argument(
        '-p', '--plot', action='store_true', dest='show',
        help="Show plots interactively"
    )
    parser.add_argument(
        '-f', '--mlp-file', dest='mlp_file', default=None,
        help="Load MLP from this file rather than training"
    )

    # parse command line arguments
    args = parser.parse_args()

    # read results from csv
    print('Loading results from ', args.ifile)
    # csv format: a,b,c,d, power
    # for multiplication a*b followed by c*d, all 5 bit signed numbers
    df = pd.read_csv(
        args.ifile,
        names=['a', 'b', 'c', 'd', 'en'],
        dtype=dict(a='int8', b='int8', c='int8', d='int8', en='float32')
    )

    print('Generating metrics')
    # Apply metrics
    metrics = []
    for v in ['a', 'b', 'c', 'd']:
        name = 'Zeroes %s' % (v.upper())
        metrics.append(name)
        df[name] = df[v].apply(zeroes)

    for (v0, v1) in (('a', 'c'), ('b', 'd')):
        sname = v0.upper()+v1.upper()
        # Hamming
        name = 'Hamming %s' % (sname)
        metrics.append(name)
        df[name] = df.apply(
            lambda row: hamming(int(row[v0]), int(row[v1])), axis=1
        )

        # Leading matching bits
        name = 'Leading %s' % (sname)
        metrics.append(name)
        df[name] = df.apply(
            lambda row: lead_match(int(row[v0]), int(row[v1])), axis=1
        )

    # Use to save computed metrics
    #  df.to_feather('saved')
    #  df = pd.read_feather('saved')

    energy = df['en']
    # Get Ideal Figure
    H = heatmap(energy, energy, metricname='Energy',
                fname=args.energy, show=args.show)

    # Calculate direct correlation between the metrics and the energy
    print('Calculate direct metric correlations')
    for name in metrics:
        print('Correlation %s % 2.5f' % (name, correlate(df['en'], df[name])))

    # lin regression
    from sklearn import linear_model
    lm = linear_model.LinearRegression()
    model = lm.fit(df[metrics], energy)

    # predict
    predicted = lm.predict(df[metrics])
    print('Learned coefficients')
    for coef, name in zip(lm.coef_, metrics):
        print(name, str(coef))

    # R2 Score
    print('Linear Regression R2: ', r2_score(energy, predicted))
    print('Correlation: ', correlate(predicted, energy))

    # Get Figure
    H = heatmap(energy, predicted, metricname='Lin.Reg.',
                fname=args.linreg, show=args.show)

    # Create MLP
    if args.mlp_file:
        # load from previously saved model
        mlp = MLP(fname=args.mlp_file)
    else:
        # create new network
        w = len(metrics)
        mlp = MLP(hidden_layers=(
            8*w, 16*w, 16*w, 16*w, 16*w, 16*w, 16*w,
        ))

        # learn
        mlp.fit(df[metrics], energy)

        # save the learned model
        mlp.save('xe_mlp.p')

    # evaluatie the MLP
    print('Predicting')
    predicted = mlp.eval(df[metrics])

    # Get the R2 Score
    print('MLP R2: ', r2_score(energy, predicted))
    print('Correlation: ', correlate(predicted, energy))

    # Get figure
    H = heatmap(energy, predicted, fname=args.mlp, show=args.show)
