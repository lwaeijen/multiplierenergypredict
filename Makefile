# Sets up a virtual python environment
VENV=.venv
ACT=./activate

ACTIVATE_SCRIPT=$(VENV)/bin/activate
INSTALL_DONE=$(VENV)/.install_done
VENV_PACKAGES=.pythonpackages

predict:predict.py energy.csv $(INSTALL_DONE)
	bash -c "source $(ACT) && ./$<"

#######################################################
#
# Generic help target. All targets followed by double # will be printed
#
help: ## Show this help.
	@grep -h '##' $(MAKEFILE_LIST) | grep -v '###' | grep -v grep | sed -e 's/\\$$//' | sed -e 's/:.*##/: /'

#######################################################
#
# targets for installation of the virtual environment
#
.PHONY:install
install:$(INSTALL_DONE) ## Install python virtual environment with all required packages
$(INSTALL_DONE):$(ACT) $(VENV_PACKAGES)
	# In particular on our bare bone CI test runners the setuptools can be outdated without forcing an update
	bash -c "source $(ACT) && pip install -U pip wheel setuptools && pip install -r $(VENV_PACKAGES) && touch $@"

$(ACT):$(ACTIVATE_SCRIPT)
	ln -fs $< $@

$(ACTIVATE_SCRIPT):
	bash -c "export LC_ALL=C && virtualenv -p python3 $(VENV) || ( rm -rf $(VENV); exit 1 )"

###########
#
# Cleaning of the Virtual Env
#
.PHONY:clean
clean: ## Clean everything including the virtual environment leaving a clean repository
	rm -rf $(VENV) $(ACT)
