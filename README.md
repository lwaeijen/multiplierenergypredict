# Multiplier Energy Analysis Tool
------
Multiplication is an important operation in modern computing systems.
Compared to most other arithmetic operations it consumes a significant amount of energy, despite dedicated hardware support for multiplication.
The materials in this repository are an attempt to better understand the energy consumption of multiplier circuits, and possibly find weak points in the circuit that can be further optimized.

`energy.csv` contains exhaustive energy measurements for an integer 5x5 bit, signed baugh-wooley multiplier.
Since the energy consumed depends on both the operands of a multiplication _and_ the initial state of the circuit, two multiplications, i.e. AxB and CxD, are performed consecutively, and the energy for CxD is measured.
These measurements are stored in "energy.csv", in the form "A,B,C,D,Energy" for a multiplication AxB followed by CxD, and the energy in pico Joules.
Details on how these numbers are obtained can be found in (todo: reference paper)

`predict.py` uses linear regression and a multi-layer preceptron to model the energy distribution using the following metrics on the operands:
- number of zeroes
- hamming distance between A,C and B,D
- number of matching lead bits in A,C and B,D
The energy range is divided into 16 bins, and the classifier is trained to predict the energy class based on the operands metrics.

In particular this repository shows that a simple machine learning techniques can reasonably predict the energy consumption based on the presented metrics. The is also room for improvement however, possibly by introducing new metrics, or more advanced machine learning, which is partly why we open source this repository. An open invitation to everyone out there interested in the topic to use our measurements, and come up with better models and metrics that help understand the inefficiencies in multiplier circuits, and possibly how to improve them.

## Installation
Any debian based OS should be sufficient to run this code.
Requirements are a python2 interpreter (tested with 2.7), virtualenv and make.
Both can be installed on ubuntu / debian by executing:
`sudo apt install python2.7 virtualenv make`

If all requirements are met, you can simply install and run with:
```make```

